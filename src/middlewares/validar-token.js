/**
 * Create by wjwebdevelopemnts on 20201026 6:15:00
 */
import { response, request } from 'express';
import jwt from 'jsonwebtoken';

export const VALIDAR_TOKEN = (req = request, res = response, next) =>
{

    const TOKEN = req.header('x-token');

    if ( !TOKEN ) 
    {
        return res.status(401).json({
            ok: false,
            error: {
                err: 'Acceso no autorizado',
                msg: 'Debes de registrarte para acceder a este servicio'
            }
        })
    }

    try {

        // TODO: Obtener role
        // Obten el role del usuario que se registra directamente desde el token
        const { uid } = jwt.verify(TOKEN, process.env.JWT_SECRET);
        req.uid = uid;
        next();
        
    } catch (error) {
        return res.status(400).json({
            ok: false,
            error: {
                err: 'Token invalido',
                msg: 'El token suministrado no es valido, por favor vuelve a generar un token'
            }
        })
    }

}