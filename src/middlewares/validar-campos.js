/**
 * Create by wjwebdevelopemnts on 20201025 5:15:00
 */
import { validationResult } from 'express-validator'; 
import { response } from 'express';
 
export const VALIDAR_CAMPOS = (req, res = response, next) => {

    // Catch validator errors, create array in req with all errors
    const errores = validationResult(req);

    if (!errores.isEmpty()) {
        return res.status(400).json({
            ok: false,
            errors: errores.mapped()
        });
    }

    next();
}

