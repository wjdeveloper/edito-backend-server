/**
 * /api/users
 * Create by wjwebdevelopments on 20201026 09:46:00
 */

import Router from 'express';
import { VALIDAR_CAMPOS } from '../middlewares/validar-campos';
import { VALIDAR_TOKEN } from '../middlewares/validar-token';
import { check } from 'express-validator';
const router = Router();

import { GET_USERS, CREATE_USER, UPDATE_USER, DELETE_USER } from '../controllers/users';


router.get( '/', 
    [
        VALIDAR_TOKEN
    ], 
    GET_USERS 
);

router.post( '/create', 
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('password', 'La contraseña es obligatoria').not().isEmpty(),
        check('email', 'El correo es obligatorio').isEmail(),
        VALIDAR_CAMPOS,
    ],
    CREATE_USER
);

router.put('/update/:id', 
    [
        VALIDAR_TOKEN,
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('email', 'El correo es obligatorio').isEmail(),
        check('role', 'El role es obligatorio').not().isEmpty(),
        VALIDAR_CAMPOS,
    ],
    UPDATE_USER    
);

router.delete('/delete/:id', VALIDAR_TOKEN, DELETE_USER);

export default router;