/**
 * path: /api/posts
 * Create by wjwebdevelopments on 20201027 09:09:00PM
 * @Post_router
 */

import { Router } from 'express';
import { check } from 'express-validator';
const router = Router();

import { GET_POSTS, CREATE_POST } from '../controllers/posts';
import { VALIDAR_CAMPOS } from '../middlewares/validar-campos';
import { VALIDAR_TOKEN } from '../middlewares/validar-token';

router.get('/', GET_POSTS);

router.post('/create', 
    [
        check('body', 'Debes de añadir contenido al cuerpo').not().isEmpty(),
        check('title', 'El titulo es obligatorio').not().isEmpty(),
        check('tags', 'Debes de agregar al menos una etiqueta').not().isEmpty(),
        VALIDAR_CAMPOS,
        VALIDAR_TOKEN
    ],
    CREATE_POST
);

export default router;