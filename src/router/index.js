/**
 * path: /
 * Create by wjwebdevelopments on 20201025 10:30
 */

import Router from 'express';
const router = Router();

import { VALIDAR_TOKEN } from '../middlewares/validar-token';
import { GET_INDEX } from '../controllers/index';

router.get( '/', VALIDAR_TOKEN, GET_INDEX);

export default router;