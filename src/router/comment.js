/**
 * path: /api/comments
 * Create by wjwebdevelopments on 20201027 09:09:00PM
 * @Comment_router
 */

import { Router } from 'express';
import { check } from 'express-validator';
const router = Router();

import { GET_COMMENTS, CREATE_COMMENT } from '../controllers/comment';
import { VALIDAR_CAMPOS } from '../middlewares/validar-campos';
import { VALIDAR_TOKEN } from '../middlewares/validar-token';

router.get('/', GET_COMMENTS);

router.post('/create', 
    [
        check('text', 'Debes de introducir tu comentario').not().isEmpty(),
        check('post', 'El post id debe de ser válido').isMongoId(),
        VALIDAR_CAMPOS,
        VALIDAR_TOKEN
    ],
    CREATE_COMMENT
);

export default router;