/**
 * Path: /api/login
 * Create by wjwebdevelopemnts on 20201025 6:56:00
 */
import Router from 'express';
import { check } from 'express-validator';
import { VALIDAR_CAMPOS } from '../middlewares/validar-campos';
const router = Router();

import { AUTH } from '../controllers/auth';

router.post( '/', 
    [
        check('email', 'El correo es obligatorio').isEmail(),
        check('password', 'La contraseña es obligatoria').not().isEmpty(),
        VALIDAR_CAMPOS,
    ],
    AUTH
);

export default router;