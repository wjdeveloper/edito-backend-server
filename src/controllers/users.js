import User from '../models/user';
import bcrypt from 'bcryptjs';

import { response } from 'express';
import { generarJWT } from '../helpers/jwt';

// GET USERS
export const GET_USERS = async (req, res) => 
{
    let userDB = await User.find({}, 'name email google role avatar password');

    res.send({
        ok: true,
        msg: 'Lista de usuarios encontrados!',
        users: userDB
    });
}

// CREATE USER
export const CREATE_USER = async (req, res = response) => 
{
    let { name, email, password } = req.body;

    try {

        let existEmail = await User.find({ email });
        
        if (existEmail.length > 0) {
            return res.status(400).json({
                ok: false,
                email: {
                    error: {
                        msg: 'El correo ya esta registrado'
                    }
                }
            });
        }

        let user = new User({name, email, password});

        // Password encrypting
        let salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(password, salt);
        
        await user.save();
        
        // Generate jwt user
        let token = await generarJWT( user.id );

        // Send the response to client
        res.json({ ok: true, user, token });

    } catch (error) {
        return res.status(500).send({
            ok: false,
            error: 'Error inesperado al crear el usuario'
        });
    }

}

// UPDATE USER
export const UPDATE_USER = async (req, res = response) => 
{
    
    // TODO: Validar token y comprobar si el usuario es correcto

    let uid = req.params.id;

    try {

        let userDB = await User.findById(uid);

        if (!userDB) {
            return res.status(400).json({
                ok: false,
                msg: 'El usuario no se encuentra registrado'
            });
        }

        let { password, email, google, ...campos } = req.body;

        if ( userDB.email !== email ) {

            let existEmail = await User.findOne({ email });

            if ( existEmail ) {

                return res.status(400).json({
                    ok: false,
                    msg: 'Ya esxite un usuario con este email registrado'
                });

            }

        }
        
        // Agrege email to update campos data
        campos.email = email;

        // Find userDB and update with campos data { new: true } to send new user updated to client
        let userUpdated = await User.findOneAndUpdate(uid, campos, { new: true });
        
        res.json({
            ok: true,
            msg: `El usuario fue actualizado con exito`,
            user: userUpdated
        });

    } catch (error) {

        console.log(error);

        return res.status(500).json({
            ok: false,
            error: 'Error inesperado al actualizar el usuario'
        });
    }
}

// DELETE USER
export const DELETE_USER = async (req, res = response) => 
{

    let uid = req.params.id;

    try {

        let userDB = await User.findById(uid);

        if ( !userDB ) {
            return res.status(400).json({
                ok: false,
                message: 'El usuario no se encuentra registrado'
            })
        };

        let userDelete = await User.findByIdAndDelete(uid);
        
        res.json({
            ok: true,
            msg: `El usuario ${ userDelete.name } ha sido eliminado con éxito`
        })
        
    } catch (error) {

        console.log(error);

        throw new Error('Error inesperado al eliminar el usuario');
        
    }


}

