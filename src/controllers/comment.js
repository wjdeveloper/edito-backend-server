/**
 * Create by wjwebdevelopments on 20201027 09:09:00PM
 * @Comment_controller
 */

import Comment from '../models/comment'; 
import { response } from 'express';

export const GET_COMMENTS = async (req, res = response, next) =>
{
    
    try {

        let commentsDB = await Comment.find()
            .populate('author', 'name avatar')
            .populate('post', 'title');
        
        res.json({
            ok: true,
            msg: 'Listado de comentarios',
            comments: commentsDB
        });

    } catch (error) {
        
        console.log(error);

        res.json({
            ok: false,
            error: {
                msg: '500 Error de servidor interno'
            }
        });
    }
}

export const CREATE_COMMENT = async (req, res = response, next) =>
{

    const uid = req.uid;
    let comment = new Comment({
        ...req.body,
        author: uid
    });

    try {
        
        let commentDB = await comment.save();

        res.json({
            ok: true,
            msg: 'Se ha creado un nuevo comentario',
            comment: commentDB
        });
        
    } catch (error) {
        
        console.log(error);

        res.json({
            ok: false,
            error: {
                msg: '500 Error de servidor interno'
            }
        });
    }


}