/**
 * Create by wjwebdevelopemnts on 20201025 6:56:00
 */

import { response } from 'express';
import User from '../models/user';
import { generarJWT } from '../helpers/jwt';
import bcrypt from 'bcryptjs';

export const AUTH = async (req, res = response) => 
{

    let { email, password } = req.body;

    try {

        let userDB = await User.findOne({ email });
        
        // Verificar correo
        if ( !userDB ) {
            return res.status(400).json({
                ok: false,
                errors: {
                    email: {
                        msg: 'El correo no existe'
                    }
                }
            });
        }

        // Verificar contraseña
        let validPassword = bcrypt.compareSync(password, userDB.password);
        if( !validPassword ) {
            return res.status(400).json({
                ok: false,
                errors: {
                    password: {
                        msg: 'La constraseña es incorrecta'
                    }
                }
            })
        }

        // Generar un token con jwt
        let token = await generarJWT( userDB._id, userDB.role );

        res.json({
            ok: true,
            msg: 'Usuario autenticado',
            token
        });

    } catch (error) {

        console.log(error);

        res.status(500).json({
            ok: false,
            errors: {
                email: {
                    msg: 'Prohibido el acceso, comuniquese con el administrador',
                    error
                }
            }
        });

    }
}