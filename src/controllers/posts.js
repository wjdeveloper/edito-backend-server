/**
 * Create by wjwebdevelopments on 20201027 09:09:00PM
 * @Post_controller
 */

import Post from '../models/post'; 
import { response } from 'express';

export const GET_POSTS = async (req, res = response, next) =>
{
    try {

        let postsDB = await Post.find()
            .populate('author', 'name email avatar role');
        
        res.json({
            ok: true,
            msg: 'Listado de posts',
            posts: postsDB
        });

    } catch (error) {
        return res.status(500).json({
            ok: false,
            error: {
                msg: '500 Error de servidor interno'
            }
        });
    }
}

export const CREATE_POST = async (req, res = response, next) =>
{
    
    let uid = req.uid;

    let post = new Post({
        ...req.body,
        author: uid
    });

    try {

        await post.save();

        res.json({
            ok: true,
            msg: 'Se ha creado un nuevo post',
            post
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            error: {
                err: '500 Error de servidor interno'
            }
        });
    }

}