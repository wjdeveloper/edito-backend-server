import app from './index';
import colors from 'colors';

import dbConnection from './database/config';

const App = async () => 
{
    try {
        await app.listen( app.get('port') );
        dbConnection();
        return `Server running on port ${colors.green( app.get('port'))}`;
    } catch (error) {
        throw new Error(error);
    }
}

App()
    .then(console.log)
    .catch(error => console.error(error.message));
