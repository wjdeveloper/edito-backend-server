/**
 * Create by wjwebdevelopments on 20201027 08:41:00PM
 * @User mongodb collection
 */

import { Schema, model } from 'mongoose';

const USER_SCHEMA = Schema({

    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    avatar: {type: String, default: null},
    role: {type: String, required: true, default: 'USER_ROLE'},
    google: {type: Boolean, default: false}
    
}); 

USER_SCHEMA.method('toJSON', function() {
    const { __v, _id, password, ...objects } = this.toObject();
    objects.uid = _id;
    return objects; 
})

export default model('User', USER_SCHEMA);