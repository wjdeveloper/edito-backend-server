/**
 * Create by wjwebdevelopments on 20201027 08:41:00PM
 * @Post mongodb collection
 */

import { Schema, model } from 'mongoose';
import moment from 'moment';

const POST_SCHEMA = Schema({

    body: { type: String, required: true },
    title: { type: String, required: true },
    date: { type: String, default: moment().format('YYYYMMDD, h:mm:ss a') },
    author: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    tags: { type: Array, required: true },
    comments: { type: Array, default: [] },
    images: { type: Array, default: [] }

}, { collection: 'posts' });

POST_SCHEMA.method('toJSON', function() {

    let { __v, ...objects } = this.toObject();
    return objects;
});

export default model('Post', POST_SCHEMA);