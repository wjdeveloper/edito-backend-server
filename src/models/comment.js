/**
 * Create by wjwebdevelopments on 20201027 08:59:00PM
 * @Comment mongodb collection
 */

 import { Schema, model } from 'mongoose';
 import moment from 'moment';
 
 const POST_COMMENT = Schema({

    text: { type: String, required: true },
    author: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    post: { type: Schema.Types.ObjectId, ref: 'Post', required: true},
    date: { type: String, default: moment().format('YYYYMMDD, h:mm:ss a') }

 }, { collection: 'comments' });

 POST_COMMENT.method('toJSON', function() {

    let { ...objects } = this.toObject();
    
    return objects;
 });

 export default model('Comment', POST_COMMENT);