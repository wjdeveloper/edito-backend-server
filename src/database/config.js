import mongoose from 'mongoose';

import colors from 'colors';

require('dotenv').config();

const dbConnection = async () => {

    try {

        // Database Connection
        // await mongoose.connect(`mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.jnklj.mongodb.net/${process.env.DB_NAME}`
        await mongoose.connect(`${process.env.DB_LOCAL}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        });

        // Database connection successfull message
        console.log(`Base de datos ${colors.green('online')}`);

    } catch (error) {

        // Database connection failed message and action
        console.log(error);
        throw new Error(`Error a la hora de iniciar la base de datos ver logs: ${error.message}`);

    }
    
}

export default dbConnection;