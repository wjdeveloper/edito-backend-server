import '@babel/polyfill';
import dotenv from 'dotenv';
import expres from 'express';
import cors from 'cors';

// Config
dotenv.config();
const app = expres();
app.use(cors());
app.use(expres.urlencoded({ extended: false }));
app.use(expres.json());

// Settings
app.set('port', process.env.PORT || 3000);

// Router
import indexRouter from './router/index';
import userRouter from './router/users';
import authRouter from './router/auth';
import postRouter from './router/posts';
import commentRouter from './router/comment';

// Use router
app.use('/', indexRouter);
app.use('/api/users', userRouter);
app.use('/api/login', authRouter);
app.use('/api/posts', postRouter);
app.use('/api/comments', commentRouter);


export default app;